AWS SNS
=======
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create SNS topics with Cloudformation. AWS resources that will be created are:
 * SNS topic

See https://docs.aws.amazon.com/sns/latest/api/API_Subscribe.html for the available endpoints and protocols.

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x

Required python modules:
* boto
* boto3
* awscli

Dependencies
------------
 * None

Role Variables
--------------
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
environment_type: <environment>
environment_abbr: <environment abbriviation>
```

Role Defaults
-------------
```yaml
---
create_changeset   : True
debug              : False
cloudformation_tags: {}
tag_prefix         : "mcf"

aws_sns_params:
  stack_name      : "{{ slicename | default(environment_abbr) }}-sns"
  create_changeset: "{{ create_changeset }}"
  debug           : "{{ debug }}"
  slicename       : "{{ slicename | default(environment_abbr) }}"

  topics: {}

    ## Example topic configuration
    # topic1:
    #   displayname: "This topic is used for ..."
    #   subscriptions:
    #     - endpoint: "example@example.com"
    #       protocol: "email"
    # topic2:
    #   displayname: "This topic is used for ..."
    #   subscriptions:
    #     - endpoint: "(123) 456-7890"
    #       protocol: "sms"
    # topic3:
    #   topicname: "{{ environment_abbr + '-' + testSNS }}"
    #   displayname: "Some env topic with override topicname..."
    #   subscriptions:
    #     - endpoint: "example@example.com"
    #       protocol: "email"

    ####### Only use if you want to encrypt your sns queue
    #   kmsmasterkeyid              : "alias/aws/sns"
```

Example Playbook
----------------
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"
    slicename       : "tst1"

    aws_sns_params:
      topics:
        topic1:
          displayname: "This topic is used for ..."
          subscriptions:
            - endpoint: "example@example.com"
              protocol: "email"
        topic2:
          displayname: "This topic is used for ..."
          subscriptions:
            - endpoint: "(123) 456-7890"
              protocol: "sms"

  roles:
    - aws-sns
```

License
-------
GPLv3

Author Information
------------------
Christiaan Druif <cdruif@mirabau.nl>  
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
